import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'bachelorapp';

  get fibo() {
    const n = 3;
    return this.fibo(n - 2) + this.fibo(n - 1);
}
}
